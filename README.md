# CIS212 FINAL PROJECT

Word frequency counter which can take a set of files or stdin.

Valid flags for the counter are: a, f, l, i, and p. These equate to 
alphabetical ordering, numerical ordering, case insensitive, ivert 
order, and break on punctuation, respectively. 


### NOTE

In order to run test suite, go to test suite and run tscript.

CFLAGS=-W -Wall -I.
LDFLAGS=-L.
PROGRAMS=wordfreq
OBJECTS=sort.o
LIBRARIES=-lADTs

wordfreq: wordfreq.o sort.o
	gcc $(LDFLAGS) -o wordfreq $^ $(LIBRARIES)

wordfreq.o: wordfreq.c sort.h

clean:
	rm -f $(PROGRAMS) $(OBJECTS) wordfreq.o

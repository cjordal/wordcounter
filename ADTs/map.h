#ifndef _MAP_H_
#define _MAP_H_

/*
 * Copyright (c) 2018, University of Oregon
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of the University of Oregon nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ADTs/iterator.h"               /* needed for factory method */

/* interface definition for generic map implementation */

typedef struct map Map;             /* forward reference */
typedef struct mentry MEntry;       /* opaque type definition */

/* create a hashmap
 *
 * returns a pointer to the hashmap, or NULL if there are malloc errors
 *
 * the hash function pointer is applied to a key to yield a bucket index
 * the cmp function pointer is applied to a pair of keys, yielding <0 | 0 | >0
 */
const Map *Map_create(long capacity, double loadFactor,
                      long (*hash)(void*, long N), int (*cmp)(void*, void*));

/* now define struct map */
struct map {
/* the private data for the map */
    void *self;

/* destroys the map; for each (key,value) pair in the map:
 *    if freeK != NULL, it is invoked on the key
 *    if freeV != NULL, it is invoked on the value
 * the storage associated with the map is returned to the heap */
    void (*destroy)(const Map *m, void (*freeK)(void*), void (*freeV)(void*));

/* clears all (key,value) pairs from the map; freeK and freeV are applied as
 * in destroy above; any storage associated with the (key,value) pair is
 * returned to the heap */
    void (*clear)(const Map *m, void (*freeK)(void*), void (*freeV)(void*));

/* returns 1 if key is contained in the map, 0 if not */
    int (*containsKey)(const Map *m, void *key);

/* returns the value associated with key in *value; returns 1 if key was found
 * in the map, 0 if not */
    int (*get)(const Map *m, void *key, void **value);

/* puts (key,value) into the map; if there was a previous value associated with
 * key in the map, the previous key and value are returned in *prevK and *prevV;
 * if not, then NULL is returned in prevK
 *
 * returns 1 if (key,value) was successfully stored in the map, 0 if not */
    int (*put)(const Map *m, void *key, void *value, void **prevK, void **prevV);

/* puts (key,value) into the map iff the map does not contain a value associated
 * with key
 *
 * returns 1 if (key,value) was successfully stored in the map, 0 if not */
    int (*putUnique)(const Map *m, void *key, void *value);

/* removes the (key,value) pair from the map, returning the stored key and value
 * in *theK and *theV
 *
 * returns 1 if (key,value) was present and removed, 0 if it was not present */
    int (*remove)(const Map *m, void *key, void **theK, void **theV);

/* returns the number of (key,value) pairs in the map */
    long (*size)(const Map *m);

/* returns 1 if the map is empty, 0 if not */
    int (*isEmpty)(const Map *m);

/* returns an array containing all of the keys in the map; the order is
 * arbitrary; returns the length of the array in *len
 *
 * returns a pointer to the void * array of keys, or NULL if malloc failure
 *
 * NB - the caller is responsible for freeing the void * array when finished
 * with it */
    void **(*keyArray)(const Map *m, long *len);

/* returns an array containing all of the (key,value) pairs in the map;
 * the order is arbitrary; returns the length of the array in *len
 *
 * returns a pointer to the MEntry * array of (k,v) pairs,
 * or NULL if malloc failure
 *
 * NB - MEntry is an opaque data type; the caller must use the mentry_*
 * functions defined below.
 *
 * NB - the caller is responsible for freeing the MEntry * array when finished
 * with it */
    MEntry **(*entryArray)(const Map *m, long *len);

/* create generic iterator to the map
 *
 * returns pointer to the Iterator or NULL if malloc failure
 *
 * NB - when the next() method on the iterator is called, it returns an
 *      MEntry *; the key and value can be obtained from the MEntry * using
 *      the mentry_* functions defined below. */
    const Iterator *(*itCreate)(const Map *m);
};

/* functions to use to obtain the key or value from an MEntry * */
void *mentry_key(MEntry *me);
void *mentry_value(MEntry *me);

#endif /* _MAP_H_ */

#ifndef _CSKMAP_H_
#define _CSKMAP_H_

/*
 * Copyright (c) 2018, University of Oregon
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of the University of Oregon nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ADTs/iterator.h"               /* needed for factory method */

/* interface definition for generic map implementation */

typedef struct cskmap Map;       /* forward reference */
typedef struct mentry MEntry;       /* opaque type definition */

/* create a map in which the keys are C strings
 *
 * returns a pointer to the map, or NULL if there are malloc errors
 */
const Map *CSKMap_create(long capacity, double loadFactor);

/* now define struct cskmap */
struct cskmap {
/* the private data for the map */
    void *self;

/* destroys the map; for each (key,value) pair in the map:
 *    if freeV != NULL, it is invoked on the value
 * the storage associated with the map is returned to the heap */
    void (*destroy)(const Map *m, void (*freeV)(void*));

/* clears all (key,value) pairs from the map; freeV is applied as
 * in destroy above; any storage associated with the (key,value) pair is
 * returned to the heap */
    void (*clear)(const Map *m, void (*freeV)(void*));

/* returns 1 if key is contained in the map, 0 if not */
    int (*containsKey)(const Map *m, char *key);

/* returns the value associated with key in *value; returns 1 if key was found
 * in the map, 0 if not */
    int (*get)(const Map *m, char *key, void **value);

/* puts (key,value) into the map; if there was a previous value associated with
 * key in the map, the previous value is returned in *prevV;
 * if not, then NULL is returned in prevV
 *
 * a copy of the key is made in the implementation, so the user does not have
 * to provide keys that have been allocated on the heap
 *
 * returns 1 if (key,value) was successfully stored in the map, 0 if not */
    int (*put)(const Map *m, char *key, void *value, void **prevV);

/* puts (key,value) into the map iff the map does not contain a value associated
 * with key
 *
 * a copy of the key is made in the implementation, so the user does not have
 * to provide keys that have been allocated on the heap
 *
 * returns 1 if (key,value) was successfully stored in the map, 0 if not */
    int (*putUnique)(const Map *m, char *key, void *value);

/* removes the (key,value) pair from the map, returning the stored value
 * in *theV
 *
 * returns 1 if (key,value) was present and removed, 0 if it was not present */
    int (*remove)(const Map *m, char *key, void **theV);

/* returns the number of (key,value) pairs in the map */
    long (*size)(const Map *m);

/* returns 1 if the map is empty, 0 if not */
    int (*isEmpty)(const Map *m);

/* returns an array containing all of the keys in the map; the order is
 * arbitrary; returns the length of the array in *len
 *
 * returns a pointer to the char * array of keys, or NULL if malloc failure
 *
 * NB - the caller is responsible for freeing the char * array when finished
 * with it */
    char **(*keyArray)(const Map *m, long *len);

/* returns an array containing all of the (key,value) pairs in the map;
 * the order is arbitrary; returns the length of the array in *len
 *
 * returns a pointer to the MEntry * array of (k,v) pairs,
 * or NULL if malloc failure
 *
 * NB - MEntry is an opaque data type; the caller must use the mentry_*
 * functions defined below.
 *
 * NB - the caller is responsible for freeing the MEntry * array when finished
 * with it */
    MEntry **(*entryArray)(const Map *m, long *len);

/* create generic iterator to the map
 *
 * returns pointer to the Iterator or NULL if malloc failure
 *
 * NB - when the next() method on the iterator is called, it returns an
 *      MEntry *; the key and value can be obtained from the MEntry * using
 *      the mentry_* functions defined below. */
    const Iterator *(*itCreate)(const Map *m);
};

/* functions to use to obtain the key or value from an MEntry * */
char *cskmentry_key(MEntry *me);
void *cskmentry_value(MEntry *me);

#endif /* _CSKMAP_H_ */

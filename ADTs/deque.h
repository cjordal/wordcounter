#ifndef _DEQUE_H_
#define _DEQUE_H_

/*
 * Copyright (c) 2018, University of Oregon
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of the University of Oregon nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include "ADTs/iterator.h"                   /* needed for factory method */

/* interface definition for generic deque implementation */

typedef struct deque Deque;             /* forward reference */

/* create a deque
 *
 * returns a pointer to the deque, or NULL if there are malloc() errors
 */
const Deque *Deque_create(void);

/* now define struct deque */
struct deque {
/* the private data for the deque */
    void *self;

/* destroys the deque; for each element in the deque, if freeFxn != NULL,
 * it is invoked on that element; the storage associated with the deque
 * is then returned to the heap */
    void (*destroy)(const Deque *d, void (*freeFxn)(void *element));

/* clears all elements from the deque; for each element, if freeFxn != NULL,
 * is invoked on that element; any storage associated with the element in
 * the deque is then returned to the heap */
    void (*clear)(const Deque *d, void (*freeFxn)(void *element));

/* inserts element at the head of the deque
 *
 * returns 1 if successful, 0 if unsuccessful (malloc errors) */
    int (*insertFirst)(const Deque *d, void *element);

/* inserts element at the tail of the deque
 *
 * returns 1 if successful, 0 if unsuccessful (malloc errors) */
    int (*insertLast)(const Deque *d, void *element);

/* returns the element at the head of the deque in *element
 *
 * returns 1 if successful, 0 if the deque is empty */
    int (*first)(const Deque *d, void **element);

/* returns the element at the tail of the deque in *element
 *
 * returns 1 if successful, 0 if the deque is empty */
    int (*last)(const Deque *d, void **element);

/* removes the element at the head of the deque, returning it in *element
 *
 * returns 1 if successful, 0 if the deque is empty */
    int (*removeFirst)(const Deque *d, void **element);

/* removes the element at the tail of the deque, returning it in *element
 *
 * returns 1 if successful, 0 if the deque is empty */
    int (*removeLast)(const Deque *d, void **element);

/* returns the number of elements in the deque */
    long (*size)(const Deque *d);

/* returns 1 if the deque is empty, 0 if it is not */
    int (*isEmpty)(const Deque *d);

/* returns an array containing all of the elements of the deque in
 * proper sequence (from head to tail); returns the length of the array
 * in *len
 *
 * returns a pointer to the void * array of elements, or NULL if malloc failure
 *
 * NB - the caller is responsible for freeing the void * array when finished
 * with it */
    void **(*toArray)(const Deque *d, long *len);

/* create generic iterator to this deque
 *
 * returns pointer to the Iterator or NULL if malloc failure */
    const Iterator *(*itCreate)(const Deque *d);
};

#endif /* _DEQUE_H_ */

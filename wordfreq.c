/*  
CIS 212 Project 7
Creation of Word Frequency Table

Author: Cameron Jordal

Credits: N/A

Takes a set of files as input and generates a word frequency table as 
output.

NOTE: In the current implimentation, the program can only take 99 
files as input.
NOTE: In the current implimentation, words can not exceed 100 chars.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "sort.h"
#include "ADTs/stringADT.h"
#include "ADTs/arraylist.h"
#include "ADTs/cskmap.h"

#define UNUSED __attribute__((unused))
#define NUM_PUNCTS 32
#define MAX_FILES 30




///////////////////////////////////////
//// GETTING ARGUEMENTS
//////////////////////////////////////

typedef struct arg_data {
	int num_args;
	char **args;
}  ArgData;




//////////////////////////////////////////
//// COLLECTING VALID FLAGS
/////////////////////////////////////////

typedef struct flag_data {
	// string of valid flag chars
	char *valids;
	int n_valids;
	char *used;
	int n_used;
} FlagData;

static int valid_flag(char flag, FlagData *fd) {
	/* checks whether or not flag is in valid flags */
	// boolean to tell if character present
	int marker = 0;
	// checks if character is in valid flags 
	for (int i = 0; i < fd->n_valids; i++) {
		// increment marker if flag matches a valid flag
		if (flag == fd->valids[i]) {
			marker++;
		}
	}
	return marker;
}

static int check_flag(char *arg, FlagData *fd) {
	/* Takes an arguement and a flag data pointer.
	Identifies if the argument is a flag, and goes though 
	the elements of the flag (not including '-') checking if
	they are valid flags and adding them to used flags. 
	Returns if any invalid flags were encountered.*/
	// check if start of arg as signiture 
	int has_sig = arg[0] == '-';
	if (has_sig) {
		// get length of flag with sig 
		int arg_len = (int)strlen(arg);
		// flags can be compound flags, so go through
		// each item in flag
		for (int i = 1; i < arg_len; i++) {
			// check if flag is a valid flag
			char *sub_flag = &arg[i];
			if (valid_flag(*sub_flag, fd)) {
				// add valid flags to used and increment used number
				fd->used[fd->n_used++] = *sub_flag;
			} else {
				// print invalid flag to stderr
				fprintf(stderr, "./wordfreq: The flag \'%c\' is invalid.\n", *sub_flag);
				// stop program and signal invalid flag was used
				return 1;
			}
		}
	}
	return 0;
}

int get_flags(ArgData *ad, FlagData *fd) {
	/* Collect all the flags that get used into an 
	array and signal if any invalid flags were used */
	// look thorugh second arg to last 
	// (first arg is program name)
	for (int i = 1; i < ad->num_args; i++) {
		// check if contains flags and if they're valid
		int errors = check_flag(ad->args[i], fd);
		if (errors) {
			return 1;
		}
	}
	return 0;
}

int flag_used(char flag, FlagData *fd) {
	/* checks whether or not flag was specified 
	(in used flags) */
	// boolean to tell if flag present
	int marker = 0;
	// checks if character is in valid flags 
	for (int i = 0; i < fd->n_used; i++) {
		// increment marker if flag matches a valid flag
		if (flag == fd->used[i]) {
			marker++;
		}
	}
	return marker;
}



/////////////////////////////////
/// GETTING FILES
////////////////////////////////

typedef struct source_data {
	int files_used;
	char *filenames[MAX_FILES];
	int has_invalids;
} SourceData;

SourceData *grab_filenames(int argc, char **argv){
	/* Grab the filenames from the arguements and check to see 
	if they are valid, then return all the intel gathered. */
	// create a heap allocation to hold source data
	SourceData *sd = (SourceData *)malloc(sizeof(SourceData));
	sd->files_used = 0;
	sd->has_invalids = 0;

	for (int i = 1; i < argc; i++) {
		char *arg = argv[i];
		// check for any arguments that arent flags 
		// must otherwise be files
		if (arg[0] != '-') {
			// add useable files to filenames
			FILE *usable_file = fopen(arg, "r");
			if (usable_file) {
				fclose(usable_file);
				sd->filenames[sd->files_used++] = arg;
			}
			// otherwise don't add them and tell user that they done goofed
			else {
				fprintf(stderr, "%s: The filename '%s' is invalid.\n", argv[0], arg);
				sd->has_invalids = 1;
			}
			// tell user if they reach their file limit
			if (sd->files_used == MAX_FILES) {
				fprintf(stderr, "You hit the maximum number of files: %d.\nThe last accepted file will be: %s.\n", MAX_FILES, arg);
				return sd;
			}
		} 
	}
	return sd;
}



//////////////////////////////////////////////
/// MAKE INPUT DATA USEFUL
/////////////////////////////////////////////

typedef struct usage_info {
	// flag booleans for functions to use
	int alphabetical;
	int numerical;
	int invert;
	int lowercase;
	int punct_break;
	// count of problems with input
	int problems;
} UsageInfo;

UsageInfo *get_run_info(int argc, char *argv[]) {
	// allocate space for usage info and set defaults
	UsageInfo *ui = (UsageInfo *)malloc(sizeof(UsageInfo));
	ui->alphabetical = 0;
	ui->numerical = 0;
	ui->invert = 0;
	ui->lowercase = 0;
	ui->punct_break = 0;

	// set up arguement data
	ArgData ad = {argc, argv};
	// set up flag data
	char *valid_flags = "aflip";
	int num_valids = (int)strlen(valid_flags);
	char max_used[num_valids];
	FlagData fd = {valid_flags, num_valids, max_used, 0};

	// get flags and record if any of them were bad
	ui->problems = get_flags(&ad, &fd);

	// useage cases depending on how arguements were processed
	if (flag_used('a', &fd))
		ui->alphabetical = 1;
	if (flag_used('f', &fd))
		ui->numerical = 1;
	if (flag_used('l', &fd))
		ui->lowercase = 1;
	if (flag_used('i', &fd))
		ui->invert = 1;
	if (flag_used('p', &fd))
		ui->punct_break = 1;
	// check for any contlicting usage cases
	int ordered_chaos = ui->invert && !(ui->alphabetical || ui->numerical);
	if (ordered_chaos) {
		ui->problems += 1;
		fprintf(stderr, "%s: The flag 'i' can not be used without flags 'a' or 'f'.\n", argv[0]);
	}
	if (ui->alphabetical && ui->numerical) {
		ui->problems += 1;
		fprintf(stderr, "%s: The flags 'a' and 'f' can not be used together.\n", argv[0]);
	}

	return ui;
}



/////////////////////////////////////////////////////////////////////
/// BREAK LINES INTO WORDS AND PUT IN MAP
/////////////////////////////////////////////////////////////////////

static void replace_puncts(const String *line){
	/* Replaces all the punctuation characters with spaces */
	//  make a C string of line to operate on
	char *tmp_line = line->convert(line);
	// replace punctuation characters with spaces
	for (int i = 0; i < line->len(line); i++) {
		if (ispunct(tmp_line[i])) {
			tmp_line[i] = ' ';
		}
	}
}

static int get_line(FILE *source, const String *line, UsageInfo *run_info) {
	/* gets a line from source and adds it to line provided.
	Returns bool of if there are more lines to get */
	// make temporary string to store line from source in
	char tmp_line[BUFSIZ];
	if (fgets(tmp_line, BUFSIZ, source) != NULL) {
		// add line from source to empty string
		line->clear(line);
		line->append(line, tmp_line);
		// if -l flag was specified, make everything lowercase
		if (run_info->lowercase) {
			line->lower(line);
		}
		// if -p flag is specified, switch punct chars for spaces
		if (run_info->punct_break) {
			replace_puncts(line);
			//printf("Puncts successfully replaced.\n");
		}
		// remove excess whitespace
		line->strip(line);
		return 1;
	} 
	return 0;
}

void add_word(const Map *m, char *word) {
	/* adds a word to map. Creates new entry if word
	doesn't already exist. Adds 1 to entry in already exists. */
	if (m->containsKey(m, word)) {
		long old_val;
		long trash_val;
		m->get(m, word, (void *)&old_val);
		m->put(m, word, (void *)old_val+1, (void **)&trash_val);
	} else {
		m->putUnique(m, word, (void *)1L);
	}
}

void get_words(const Map *freqs, FILE *source, UsageInfo *run_info) {
	/* Go through source and break up each line into words
	until no lines left */
	// create an intial string to work with
	char *init_str = "";
	const String *line = String_create(init_str);

	// get the first line
	int lines_left = get_line(source, line, run_info);
	// keep getting lines until none are available
	while (lines_left) {
		// don't process empty lines (end of line char -- '/0')
		if (line->len(line) < 1) {
			lines_left = get_line(source, line, run_info);
		} 
		// otherwise lines need to be processed  
		else {
			// beak lines into words useing spaces as delineators
			const ArrayList *words = line->split(line, " ");;
			// print words in line back out on own line
			void *word[100];
			for (long i = 0; i < words->size(words); i++) {
				words->get(words, i, word);
				// add non-empty strings to frequency map
				if (strcmp((char *)*word, "") != 0) {
					add_word(freqs, (char *)*word);
				}
			}
			// get rid of old list of words after use
			words->destroy(words, free);
			// get the next line from source
			lines_left = get_line(source, line, run_info);
		}
	}
	// get rid of string after use
	line->destroy(line);
}



//////////////////////////////////////
// PRINT ENTRIES IN MAP
//////////////////////////////////////

int ab_cmp(void *e1, void *e2) {
	/* sort entries alphabetically by key */
	// compare keys of two entries
	return strcmp(cskmentry_key((MEntry *)e1), cskmentry_key((MEntry *)e2));
}

int num_cmp(void *e1, void *e2) {
	/* sort entries numerically by value */
	// compare values of two entries as ints
	return (int)(cskmentry_value((MEntry *)e2) - cskmentry_value((MEntry *)e1));
}

int i_ab_cmp(void *e1, void *e2) {
	/* sort entries alphabetically by key */
	// compare keys of two entries and give the opposite result
	return strcmp(cskmentry_key((MEntry *)e1), cskmentry_key((MEntry *)e2)) * -1;
}

int i_num_cmp(void *e1, void *e2) {
	/* sort entries numerically by value */
	// compare values of two entries as ints in opposite way of normal num_cmp
	return (int)(cskmentry_value((MEntry *)e1) - cskmentry_value((MEntry *)e2));
}

void print_map(const Map *m, UsageInfo *run_info) {
	/* prints the contents of the map */
	// create array of all entries in map
	long num_entries;
	MEntry **entries = m->entryArray(m, &num_entries);
	// sort entries as required
	if (run_info->invert) {
		if (run_info->alphabetical)
			sort((void **)entries, num_entries, i_ab_cmp);
		if (run_info->numerical)
			sort((void **)entries, num_entries, i_num_cmp);
	} else {
		if (run_info->alphabetical)
			sort((void **)entries, num_entries, ab_cmp);
		if (run_info->numerical)
			sort((void **)entries, num_entries, num_cmp);
	}
	// print each entry in array (inversely or normally)
	for (long i = 0; i < num_entries; i++) {
		printf("%8ld %s\n", (long)cskmentry_value(entries[i]), cskmentry_key(entries[i]));
	}
	// free array after use
	free(entries);
}









int main(int argc, char *argv[]) {
	// get files need for program to run
	SourceData *sd = grab_filenames(argc, argv);
	// get arguements need for program to run
	UsageInfo *ui = get_run_info(argc, argv);

	// add problems in source data to that of usage  info
	ui->problems += sd->has_invalids;

	// run program if all the arguements were good
	if (! ui->problems) {
		// make a map to hold word frequencies
		const Map *freqs = CSKMap_create(0L, 0.0);
		// get words using stdin if no files specified
		if (sd->files_used == 0) {
			get_words(freqs, stdin, ui);
		} 
		// otherwise get words from each specified file
		else {
			FILE *source;
			for (int i = 0; i < sd->files_used; i++){
				// open a file from list of valid filenames
				source = fopen(sd->filenames[i], "r");
				get_words(freqs, source, ui);
				// close each file after use
				fclose(source);
			}	
		}
		// print map after all words have been collected
		print_map(freqs, ui);
		// get rid of map after use
		freqs->destroy(freqs, NULL);
	}
	// clear up heap allocation
	free(sd);
	free(ui);
	return 0;
}



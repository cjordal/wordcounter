#ifndef _PRIOQUEUE_H_
#define _PRIOQUEUE_H_

/*
 * Copyright (c) 2018, University of Oregon
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of the University of Oregon nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ADTs/iterator.h"                      /* needed for factory method */

/* interface definition for generic priority queue implementation */

typedef struct prioqueue PrioQueue;        /* forward reference */

/* create a priority queue
 *
 * returns a pointer to the priority queue, or NULL if malloc errors */
const PrioQueue *PrioQueue_create(int (*cmp)(void*, void*));

/* now define struct prioqueue */
struct prioqueue {
/* the private data for the priority queue */
    void *self;

/* destroys the priority queue; for each element in the priority queue,
 * if freeP != NULL, it is invoked on the priority associated with the element
 * if freeE != NULL, it is invoked on the element
 * the storage associated with the priority queue is returned to the heap */
    void (*destroy)(const PrioQueue *pq, void(*freeP)(void*), void(*freeE)(void*));

/* clears all elements from the priority queue; freeP and freeE are as for
 * destroy above; any storage associated with each element is returned to
 * the heap */
    void (*clear)(const PrioQueue *pq, void (*freeP)(void*), void(*freeE)(void*));

/* inserts the element into the priority queue
 *
 * returns 1 if successful, 0 if unsuccessful (malloc errors) */
    int (*insert)(const PrioQueue *pq, void *priority, void *element);

/* returns the minimum element of the priority queue in * element
 *
 * returns 1 if successful, 0 if the priority queue is empty */
    int (*min)(const PrioQueue *pq, void **element);

/* removes the minimum element of the priority queue, returning it in *element
 *
 * returns 1 if successful, 0 if the priority queue is empty */
    int (*removeMin)(const PrioQueue *pq, void **priority, void **element);

/* returns the number of elements in the priority queue */
    long (*size)(const PrioQueue *pq);

/* returns 1 if the priority queue is empty, 0 if it is not */
    int (*isEmpty)(const PrioQueue *pq);

/* returns an array containing all of the elements of the priority queue in
 * proper sequence (smallest priority to largest priority); returns the
 * length of the array in *len
 *
 * returns a pointer to the void * array of elements, or NULL if malloc failure
 *
 * NB - the caller is responsible for freeing the void * array when finished
 * with it */
    void **(*toArray)(const PrioQueue *pq, long *len);

/* create generic iterator to this priority queue
 *
 * returns pointer to the Iterator or NULL if malloc failure */
    const Iterator *(*itCreate)(const PrioQueue *pq);
};

#endif /* _PRIOQUEUE_H_ */
